package com.stevezong.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.stevezong.crm.dao.CstCustomerMapper;
import com.stevezong.crm.entity.CstCustomer;


public class CustomerTest {

	private ApplicationContext ac;
	CstCustomerMapper dao;

	@Before
	public void init() {
		String[] conf = {"conf/spring-mvc.xml","conf/spring-mybatis.xml"};
		ac = new ClassPathXmlApplicationContext(conf);
		dao = ac.getBean("cstCustomerMapper",CstCustomerMapper.class);
	}	
	
	@Test
	public void  t1(){
		CstCustomer customer = dao.selectByPrimaryKey(1L);
		System.out.println(customer.toString());
	}
	
	@Test
	public void t2(){
		CstCustomer customer = new CstCustomer();
		customer.setCustCreateId(1L);
		customer.setCustId(1L);
		customer.setCustIndustry("s");
		customer.setCustLevel("vip");
		customer.setCustLinkman("lmdtx");
		customer.setCustMobile("13852019891");
		customer.setCustName("stevezong");
		customer.setCustPhone("110");
		customer.setCustSource("廣告");
		customer.setCustUserId(1L);
		int insert = dao.insert(customer);
		System.out.println(insert);
		
	}
	
}
