package com.stevezong.test;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.stevezong.crm.dao.CstCustomerMapper;
import com.stevezong.crm.entity.CrmResult;
import com.stevezong.crm.entity.CstCustomer;
import com.stevezong.crm.service.CustomerService;

import sun.print.resources.serviceui;


public class CustomerServiceTest {

	private ApplicationContext ac;
	@Resource(name="customerService")
	CustomerService customerService;
	
	@Before
	public void init() {
		String[] conf = {"conf/spring-mvc.xml","conf/spring-mybatis.xml"};
		ac = new ClassPathXmlApplicationContext(conf);
		customerService = ac.getBean("customerService",CustomerService.class);
	}	
	
	@Test
	public void  t1(){
		CrmResult<CstCustomer> result = customerService.findById(1L);
		System.out.println(result);
	}
	
	
	
}
