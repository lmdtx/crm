package com.stevezong.crm.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stevezong.crm.dao.CstCustomerMapper;
import com.stevezong.crm.entity.CrmResult;
import com.stevezong.crm.entity.CstCustomer;

@Service("customerService")
public class CustomerServiceImpl  implements CustomerService{

	@Resource(name="cstCustomerMapper")
	private CstCustomerMapper cstCustomerMapper;
	@Transactional
	public CrmResult<CstCustomer> findById(Long custId) {
		CrmResult<CstCustomer> result = new CrmResult<CstCustomer>();
		CstCustomer customer = cstCustomerMapper.selectByPrimaryKey(custId);
		if(customer == null){
			result.setStatus(-1);
			result.setMsg("沒有找到客戶");
		}else{
			result.setStatus(0);
			result.setMsg("success");
			result.setData(customer);
		}
		return result;
	}

}
