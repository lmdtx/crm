package com.stevezong.crm.service;

import com.stevezong.crm.entity.CrmResult;
import com.stevezong.crm.entity.CstCustomer;

public interface CustomerService {
	 CrmResult<CstCustomer>  findById(Long custId);
}
