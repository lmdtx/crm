function loadCustomer() {
	$.ajax({
		// 发送ajax请求
		url : "customer/loadCustomer",
		type : "post",
		data : {
			"pageSize" : pageSize
		},
		dataType : "json",
		success : function(result) {
			if (result.status == 0) {
				// 获取笔记本集合
				var books = result.data;
				for (var i = 0; i < books.length; i++) {
					// 获取笔记本Id
					var bookId = books[i].cn_notebook_id;
					// 获取笔记本名字
					var bookName = books[i].cn_notebook_name;
					// 创建一个笔记本列表的li元素
					createBookLi(bookId, bookName);
				}
			}
		},
		error : function() {
			alert("获取失败，请稍等重试！");
		}
	});

}

function createBookLi(bookId, bookName) {
	var sli = '<li class="online"> <a> <i class="fa fa-book" title="online" rel="tooltip-bottom"></i>'
			+ bookName + '</a></li> ';
	// alert(sli);
	// 将sli字符串转换成jq对象li元素
	// 其中$(text)就text字符串转为了一个Jquery对象
	var $li = $(sli);
	// 将bookId值与jq对象绑定
	// 向元素附加数据
	// 向被选元素附加数据。
	// $(selector).data(name,value)
	$li.data("bookId", bookId);
	// 将li元素添加到笔记本ul列表区
	$("#book_ul").append($li);
}
